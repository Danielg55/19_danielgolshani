﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _19_DanielGolshani
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void enter_Click(object sender, EventArgs e)
        {
            string userName, password;
            userName = this.userName.Text;
            password = this.password.Text;
            string text = File.ReadAllText("Users.txt");
            if (text.IndexOf(userName + "," + password) >= 0) 
            {
                this.Hide();

                string path = userName + "BD.txt";
               
                Form2 form = new Form2(path);
                form.ShowDialog();
              
                this.Close();
            }
        }
    }
}
