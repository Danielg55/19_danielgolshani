﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _19_DanielGolshani
{
    public partial class Form2 : Form
    {
        public Form2(string path)
        {
            InitializeComponent();
            _path = path;

            if (!File.Exists(path))
            {
                File.Create(path);
            }
            bd = File.ReadAllLines(path);

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date = Calendar1.SelectionStart.ToShortDateString();
            date = date.Remove(date.Length - 4);
            label1.Text = "אין ימי הולדת בתאריך הזה";
            foreach(string a in bd)        
            {
                if (a.IndexOf(date) >= 0)
                {
                    string[] tmp = a.Split(',');
                    label1.Text = "-ל" + tmp[0]  + " יש יומולדת בתאריך הזה!";
                }
            }
        }

        private void dateAdd_Click(object sender, EventArgs e)
        {
            string name = BDname.Text;
            string date = dateTimePicker1.Value.ToShortDateString();
            File.AppendAllText(_path, "\r\n"+name+","+date);

            bd = File.ReadAllLines(_path);
        }

        string _path;
        string[] bd;
    }
}
